import numpy as np

import plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt
import pylab

import scipy.stats
import scipy.optimize
import math
import sys
import sqlite3

less_powerful = input('powerful govt? (y/n): ') != 'y'

# Government policy space
if less_powerful:
    G = 12.1 # Government capacity
    g_inc = 1.0 # Government capability increments
else: # More powerful option
    G = 15.1 # Government capacity
    g_inc = 1.0 # Government capability increments

g_grid = np.arange(0.0, G, g_inc)
n_g = len(g_grid)

# Specify probability distributions
r_mean, r_range = (10.0, 0.1)
c_mean, c_range = (1.0, 0.1)
delta_mean, delta_range = (0.90, 0.02)
kappa_mean, kappa_range = (0.5, 0.1)
eta_mean, eta_range = (0.01, 0.01)


# Intercept of demand curve.
r = r_mean
# Marginal cost of quantity.
c = c_mean
# Number of traffickers to kill.
kappa = kappa_mean
# Cost of violence.
eta = eta_mean # 0.02 * kappa * (.25 * (r - c) - kappa)
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.0001
# infinity
infinity = float("inf")
zeta = 0.1
# Discount factor
delta = delta_mean
# Periods of Punishment
T = 20

# Load mean deterministic plot
deterministic_file = pylab.loadtxt('viol_plot')



# Create string identifier for this parameterization to avoid any weird floating point errors when choosing what
# results to load from database.
param_id = 'G' + str(G) + 'g_inc' + str(g_inc) + 'rm' + str(r_mean) + 'rr' + str(r_range) + 'cm' + str(c_mean) + 'cr' + str(c_range) \
           + 'dm' + str(delta_mean) + 'dr' + str(delta_range) + 'km' + str(kappa_mean) + 'kr' + str(kappa_range) \
           + 'em' + str(eta_mean) + 'er' + str(eta_range)

violence_avg = np.zeros((n_g, n_g))
violence_uncoord_pts = None
counts = np.zeros((n_g, n_g), dtype=np.int)

# Load database
dbconn = sqlite3.connect("previous_runs.db")
cursor = dbconn.cursor()

try:
    # Load any previous runs
    cursor.execute("SELECT g_bar_ind, g_tilde_ind, violence FROM runs WHERE param_id = ?", (param_id,))
    rows = cursor.fetchall()
    print('Found {} previous runs'.format(len(rows)))
except sqlite3.Error as e:
    print(e)
    sys.exit(1)

violence_uncoord_pts = np.zeros((len(rows), 2))

row_ind = 0
for row in rows:
    g_bar_ind, g_tilde_ind, viol_val = row
    counts[g_bar_ind, g_tilde_ind] = counts[g_bar_ind, g_tilde_ind] + 1
    violence_avg[g_bar_ind, g_tilde_ind] += (1./counts[g_bar_ind, g_tilde_ind]) * (viol_val - violence_avg[g_bar_ind, g_tilde_ind])

    if g_bar_ind == g_tilde_ind: # Make sure this is an uncoordinated policy
        violence_uncoord_pts[row_ind,:] = [g_grid[g_bar_ind], viol_val]
    row_ind += 1


choice = 1
n_choices = 6
while choice < n_choices:
    print("""Display:
    (1) violence means
    (2) counts
    (3) violence scatter
    (4) deterministic plot (less powerful)
    (5) deterministic plot zoomed out
    (6) exit""")
    try:
        choice = int(input(''))
    except:
        print("Invalid choice, exiting")
        choice = n_choices

    if choice == 1:
        plt.plot(g_grid, violence_avg.diagonal(), 'o')
        plt.show()

    elif choice == 2:
        plt.plot(g_grid, counts.diagonal(), 'o')
        plt.show()

    elif choice == 3:
        plt.scatter(violence_uncoord_pts[:,0], violence_uncoord_pts[:,1], alpha=0.05)
        plt.show()

    elif choice == 4:
        pylab.xlim([0,G])
        pylab.plot(deterministic_file[:,0], deterministic_file[:,1])
        plt.show()

    elif choice == 5:
        pylab.xlim([0,24])
        pylab.plot(deterministic_file[:,0], deterministic_file[:,1])
        plt.show()

