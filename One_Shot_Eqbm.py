import numpy as np
import matplotlib.pyplot as plt

# Intercept of demand curve.
r = 10.0
# Marginal cost of quantity.
c = 1.0
# Cost of violence.
eta = 0.02
# Number of traffickers to kill.
kappa = 0.3
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.00000000000001


def calculate_nash_alpha(g, alpha, lb, ub):
    payoff = calculate_payoff(g, alpha)
    if g <= mix_thresh:
        return alpha
    if (ub - lb) <= epsilon:
        return alpha
    if payoff < 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, lb, alpha)
    elif payoff > 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, alpha, ub)
    else:
        return alpha


def calculate_q(g, alpha):
    if alpha == 0:
        return 0
    else:
        return (r - c + (2 * g) + (alpha * (g + 3 * kappa))) / (2 + alpha)


def calculate_payoff(g, alpha):
    qi = calculate_q(g, alpha)

    # Calculate the utility of the case where both Cartels are 'In'
    in_price = r - 2 * (qi - g - kappa)
    in_utility = in_price * (qi - g - kappa) - c * qi - eta
    in_utility = in_utility * alpha

    # Calculate the utility of the case where both Cartels are 'In'
    out_price = r - qi + g
    out_utility = out_price * (qi - g) - c * qi
    out_utility = out_utility * (1 - alpha)
    return in_utility + out_utility


eq_alpha_array = []
eq_payoff_array = []
eq_qi_array = []
eq_qhat_array = []
g_array = np.arange(0.0, 30.0, 0.1)

for g in g_array:
    eq_alpha = calculate_nash_alpha(g, 1.0, 0, 1)
    eq_alpha_array += [eq_alpha]
    eq_payoff = eq_alpha * calculate_payoff(g, eq_alpha)
    if abs(eq_payoff) < epsilon * 10:
        eq_payoff = 0
    eq_payoff_array += [eq_payoff]
    eq_qi = calculate_q(g, round(eq_alpha, 6))
    eq_qi_array += [eq_qi]
    if eq_qi > eq_alpha * kappa + g:
        eq_qhat = eq_qi - eq_alpha * kappa - g
    else:
        eq_qhat = 0
    eq_qhat_array += [eq_qhat]

    print('g = %f, alpha = %f, E(u) = %f, qi = %f, qhat = %f'
          % (g, eq_alpha, eq_payoff, eq_qi, eq_qhat))

plt.plot(g_array, eq_alpha_array)
plt.xlabel("Arrests/firm g")
plt.ylabel("alpha")
plt.title("Equilibrium alpha v. g")
plt.grid(True)
plt.xticks(np.arange(0.0, 30.0, 2))
plt.savefig("eq_alpha.png")

plt.clf()
plt.plot(g_array, eq_payoff_array)
plt.xlabel("Arrests/firm g")
plt.ylabel("Equilibrium Payoff/firm ")
plt.title("Equilibrium Payoff v. g")
plt.grid(True)
plt.xticks(np.arange(0.0, 30.0, 2))
plt.savefig("eq_payoff.png")

plt.clf()
plt.plot(g_array, eq_qi_array)
plt.xlabel("Arrests/firm g")
plt.ylabel("Equilibrium qi")
plt.title("Equilibrium qi v. g")
plt.grid(True)
plt.xticks(np.arange(0.0, 30.0, 2))
plt.savefig("eq_qi.png")

plt.clf()
plt.plot(g_array, eq_qhat_array)
plt.xlabel("Arrests/firm g")
plt.ylabel("Equilibrium Quantity Delivered (Q hat)")
plt.title("Equilibrium Quantity Delivered v. g")
plt.grid(True)
plt.xticks(np.arange(0.0, 30.0, 2))
plt.savefig("eq_qhat.png")
