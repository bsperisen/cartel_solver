import math
import numpy
import scipy.stats, scipy.optimize

infinity = float("inf")

delta = 0.95
r = 10.
c = 1.
kappa = 0.3
qm = (r-c)/4.
eta = 0.02 * kappa * (qm - kappa)
zeta = 0.1

C0 = (5*r - 2*c) / 12.
C1 = (r + 2*c)**2 / 144.


def F(theta):
    if theta == 0:
        return 0.
    elif theta == infinity:
        return 1.
    else:
        return scipy.stats.norm.cdf((math.log(theta) + 0.5*zeta**2) / zeta)

def f(theta):
    if theta == 0 or theta == infinity:
        return 1.
    else:
        return scipy.stats.norm.pdf((math.log(theta) + 0.5*zeta**2) / zeta) / (zeta*theta)

def gamma(theta):
    return (1./6.) * (delta / (1-delta)) * f(theta) * theta if theta != infinity else 0.

def u(q, g = 0., s = 0.):
    return (r - 2*q) * q - c * (q + s + g) - s * eta

def calc_ui(qhi, qhmi, si, smi, g):
    return (r - qhi - qhmi) * qhi - c * (qhi + kappa * smi + g) - eta * si

Vm = u(qm)

# Old
# all_violence_threshold = (-2 * kappa *(c * kappa + eta) + math.sqrt(kappa * (r-c) - eta)) / (2 * c * kappa)
# all_peace_threshold = math.sqrt(kappa * (r-c)) / (2 * c * kappa)

mixing_threshold = ((1./9.) * (r-c)**2 + eta - c*kappa) / c
all_violence_threshold = (0.25*((r-c) - eta / kappa)**2 - (c*kappa + eta)) / c
all_peace_threshold = (0.25*((r-c) - eta / kappa)**2) / c


# One-shot equilibrium drugs delivered
def calc_qhatN(g = 0.):
    if g <= mixing_threshold:
        return (1./3.) * (r - c)
    else:
        return max(r - c - 2 * math.sqrt((1 + c) * eta * calc_sN(g) + c * g),0)
     
# One-shot equilibrium violence
def calc_sN(g = 0.):
    if g<= mixing_threshold:
        return 1.
    elif g > all_peace_threshold:
        return 0.
#    elif g <= all_violence_threshold:
    else:
        coeffs = [c*kappa + eta, 4*(c*kappa + eta) + c*g, 4*((c*kappa + eta) + c*g), 4*c*g - (r - c)**2]
        xi = numpy.roots(coeffs)
        for i in range(xi.size):
            xi_cand = xi.item(i)
            if numpy.isreal(xi_cand) and xi_cand >= 0.:
                return xi_cand
        print 'Should never get here!'
        exit()
#     else:
#         return (1./(c*kappa+eta)) * (0.25 * (r-c - eta/kappa)**2 - c*g) 


def calc_VN(g = 0.):
    if g <= all_violence_threshold:
        return max(u((r-c) / 3.) - c * (kappa + g) - eta,0)
    else:
        return 0.
    
def calc_Vsf(qhat, thetabar, Vbar, Vtilde, gbar = 0.):
    return (1 - delta) * u(qhat, gbar) + delta * ((1 - F(thetabar)) * Vbar + F(thetabar) * Vtilde)

def calc_qhatbar(theta, Vbar, Vtilde):
    return C0 - math.sqrt(C1 + gamma(theta) * (Vbar - Vtilde))

def calc_Vsf_qopt(theta, Vbar, Vtilde, gbar = 0.):
    return calc_Vsf(calc_qhatbar(theta, Vbar, Vtilde), theta, Vbar, Vtilde, gbar)

def is_Q_corner_sol(theta, Vbar, Vtilde):
    return C1 + gamma(theta) * (Vbar - Vtilde) < 0.        

def thetabar_is_interior(psi, thetabar, sbar, gbar, Vbar, Vtilde):
    lhs = u(psi, gbar, sbar)
    rhs = (delta / (1-delta)) * ((F(thetabar * (r - 2 * psi) / (r - psi)) - F(thetabar)) * Vbar + (F(thetabar) - F(thetabar * (r - 2*psi) / (r - psi))))
    
    # For debugging
#     if gbar >= 4:
#         print 'lhs', lhs
#         print 'rhs', rhs 
    return lhs < rhs
        
#     return (1 - delta) * u(psi, gbar, sbar) < \
#         delta * ((F(thetabar * (r - 2 * psi) / (r - psi)) - F(thetabar)) * Vbar + (F(thetabar) - F(thetabar * (r - 2*psi) / (r - psi))))

def calc_FOC(theta, Vbar, Vtilde):
    DeltaV = Vbar - Vtilde
#     if DeltaV < 0.:
#         print 'here!', theta, Vbar, Vtilde, C1 + gamma(theta) * DeltaV

    S = C1 + gamma(theta) * DeltaV
    if S < 0.:
        return - delta * f(theta) * DeltaV
    else:
        sqrtS = math.sqrt(C1 + gamma(theta) * DeltaV)
        qbar = C0 - sqrtS
        dqbarterm = -(math.log(theta) + 0.5*zeta**2) / (12*zeta**2 * sqrtS)
        today_comp = (4*qbar - (r-c)) * dqbarterm
        return today_comp - 1

def solve_FOC(Vbar, Vtilde):
    #thetabar = scipy.optimize.newton(calc_FOC, 0.8, args=(Vbar, Vtilde))
    if calc_FOC(1., Vbar, Vtilde) >= 0.:
        print 'Vbar=', Vbar, ' Vtilde=', Vtilde
        
    thetabar = scipy.optimize.brentq(calc_FOC, 0.0000001, 1., args=(Vbar, Vtilde))
#     print converged.converged
    return thetabar


def write_FOC_graph(omegatilde, gbar, gtilde, Vbar, Vtilde):
    with open('theta_graphs/FOC_' + str(gbar) + '_' + str(gtilde) + '_' + str(Vbar) + '_' + str(Vtilde), 'w') as FOC_graph:
        theta_step = 0.01
        thetabar = theta_step
        while thetabar <= 1.1:
            FOC_graph.write(str(thetabar) + ' ' + str(calc_FOC(thetabar, Vbar, Vtilde)) + "\n")
            thetabar += theta_step


# Calculates the following values for the equilibrium specified here:
# D, M, qhatbar, qhatN
# drugs, violence, drugs in reward state, drugs in punishment state
def calc_eqbm_stats(thetabar, gbar, omegatilde, gtilde, Vbar, Vtilde):
    t = 0.
    drug_sum = 0.
    violence_sum = 0.
    epsilon = 0.000001
    Prob_bar = 1.
    
    qhatbar = 0.
    sbarstate = 0
    
    # If thetabar is interior:
    if 0 < thetabar and thetabar < infinity:
        qhatbar = calc_qhatbar(thetabar, Vbar, Vtilde)
        sbarstate = 0 if 0 >= (1-delta) * (kappa * qhatbar - eta) - delta * (Vbar - Vtilde) else 1 # Repeated below (should probably make function)
    else: # Otherwise, use one-shot equilibrium.
        qhatbar = calc_qhatN(gbar)
        sbarstate = calc_sN(gbar)
    
    Prob_tilde = 0.
    qhatN = calc_qhatN(gtilde)
    stildestate = calc_sN(gtilde)
    
    Fthetabar = F(thetabar) if thetabar != infinity else 1.
    while delta**t >= epsilon:
        # Count drugs and violence.
        drug_sum += delta**t *(Prob_bar * qhatbar + Prob_tilde * qhatN)
        violence_sum += delta**t *(Prob_bar * sbarstate + Prob_tilde * stildestate)
        
        # Update probability distribution.
        Prob_bar = Prob_bar * (1 - Fthetabar) + omegatilde * Prob_tilde * Fthetabar
        Prob_tilde = 1. - Prob_bar
        
        t += 1
        
    return (1 - delta) * drug_sum, (1 - delta) * violence_sum, qhatbar, qhatN



# Returns a tuple characterizing equilibrium:
# (Vbar, thetabar, D, M, qhatbar, qhatN)
def calc_eqbm(omegatilde = 0., gbar = 0., gtilde = 0.):
    VN = calc_VN(gtilde)
#     print 'VN=', VN
    Vbar = Vm
    diff = 100.
    epsilon = 10.**(-6) # Convergence threshold
    
    # Run until converged.
    while abs(diff) >= epsilon:
        Vtilde = VN
        if omegatilde > 0.:
            T = 1. / omegatilde
            Vtilde = (1 - delta**T) * VN + delta**T * Vbar
#         write_FOC_graph(omegatilde, gbar, gtilde, Vbar, Vtilde)
        
        # If reward state is lower payoff than "punishment" state, best option is to switch immediately: set thetabar = infinity.
        if Vbar <= Vtilde:
            Vbar = (calc_VN(gbar) * (1 - delta * omegatilde) + delta * calc_VN(gtilde)) / (1 - delta * (omegatilde - 1))
            if omegatilde > 0.:
                T = 1. / omegatilde
                Vtilde = (1 - delta**T) * VN + delta**T * Vbar
                
            D, M, qhatbar, qhatN = calc_eqbm_stats(infinity, gbar, omegatilde, gtilde, Vbar, Vtilde)
            return (Vbar, infinity, D, M, qhatbar, qhatN)
        
        thetabar = solve_FOC(Vbar, Vtilde)
        psi = calc_qhatbar(thetabar, Vbar, Vtilde)
        sbar = 0 if 0 >= (1-delta) * (kappa * psi - eta) - delta * (Vbar - Vtilde) else 1 
        if thetabar_is_interior(psi, thetabar, sbar, gbar, Vbar, Vtilde):
            Vbarp = delta * (Vbar - Vtilde)
            thetabar = 0 if Vbar >= Vtilde else infinity
        Vbarp = calc_Vsf_qopt(thetabar, Vbar, Vtilde, gbar)
        diff = Vbarp - Vbar
        Vbar = Vbarp
#         print 'thetabar=', thetabar, ' Vbar=', Vbar
#     print 'output: thetabar=', thetabar, ' Vbar =', Vbar

    D, M, qhatbar, qhatN = calc_eqbm_stats(thetabar, gbar, omegatilde, gtilde, Vbar, Vtilde)
    return (Vbar, thetabar, D, M, qhatbar, qhatN)

def write_plot_2D(plot_file, G, value):
    plot_file.write(str(G) + ' ' + str(value) + "\n")

def write_plot_3D(plot_file, gtilde, gbar, value):
    plot_file.write(str(gtilde) + ' ' + str(gbar) + ' ' + str(value) + "\n")

if __name__ == '__main__':
    omegatilde = 0.05
    
    # For debugging
    print 'mixing_threshold', mixing_threshold, 'all violence', all_violence_threshold, 'all peace', all_peace_threshold
#     exit()
    
    # Manual input for testing
    while False: # Change to True when you want it.
        g = float(raw_input('Enter g'))
        print 'qN=', calc_qhatN(g), 'sN=', calc_sN(g)
        
        if False:
            gbar = raw_input("Enter gbar: ")
            if gbar == 'exit':
                break
            else:
                gbar = float(gbar)
            gtilde = raw_input("Enter gtilde: ")
            if gtilde == 'exit':
                break
            else:
                gtilde = float(gtilde)
            
            V, theta, M = calc_eqbm(omegatilde, gbar, gtilde)
            print 'Got V=' + str(V) + ', theta=' + str(theta) + ', M=' + str(M)
        
    g_step = 0.5
    gtilde = 0.
    Gmax = 22.
    
    corrupt_violence_plot = open('corrupt_violence_plot', 'w')
    corrupt_profit_plot = open('corrupt_profit_plot', 'w')
    corrupt_drug_plot = open('corrupt_drug_plot', 'w')

    crackdown_violence_plot = open('crackdown_violence_plot', 'w')
    crackdown_profit_plot = open('crackdown_profit_plot', 'w')
    crackdown_drug_plot = open('crackdown_drug_plot', 'w')
    
    
    G = 0.
    while G <= Gmax:
        # Corruption
        V, theta, D, M, qhatbar, qhatN = calc_eqbm(omegatilde, 0, G)
        Vstr = "%.4f" % V
        
        write_plot_2D(corrupt_profit_plot, G, V)
        write_plot_2D(corrupt_drug_plot, G, D)
        write_plot_2D(corrupt_violence_plot, G, M)
                    
        # Crackdown
        V, theta, D, M, qhatbar, qhatN = calc_eqbm(omegatilde, G, G)
        Vstr = "%.4f" % V
        
        write_plot_2D(crackdown_profit_plot, G, V)
        write_plot_2D(crackdown_drug_plot, G, D)
        write_plot_2D(crackdown_violence_plot, G, M)
        
        percent_done = 100. * G / Gmax
        G += g_step
        print str(percent_done) + '% done'
        
    corrupt_violence_plot.close()
    corrupt_profit_plot.close()
    corrupt_drug_plot.close()
    crackdown_violence_plot.close()
    crackdown_profit_plot.close()
    crackdown_drug_plot.close()
    print 'DONE!'

    exit() # Uncomment for 3D graph

    # Code for making the graphs
    # Open files
    violence_plot = open('violence_plot', 'w')
    profit_plot = open('profit_plot', 'w')
    drug_plot = open('drug_plot', 'w')
    qhatbar_plot = open('qhatbar_plot', 'w')
    qhatN_plot = open('qhatN_plot', 'w')
    
    
    while gtilde <= Gmax: 
        gbar = 0.
        while gbar <= Gmax:
            V, theta, D, M, qhatbar, qhatN = calc_eqbm(omegatilde, gbar, gtilde)
            Vstr = "%.4f" % V
            
            write_plot_3D(profit_plot, gtilde, gbar, V)
            write_plot_3D(drug_plot, gtilde, gbar, D)
            write_plot_3D(violence_plot, gtilde, gbar, M)
            write_plot_3D(qhatbar_plot, gtilde, gbar, qhatbar)
            write_plot_3D(qhatN_plot, gtilde, gbar, qhatN)
                        
            
            percent_done = 100. * (gtilde / Gmax + (gbar / Gmax) * g_step / Gmax)
            print str(percent_done) + '% done: gtilde=' + str(gtilde) + ', gbar=' + str(gbar) + ', V=' + str(V) + ', M=' + str(M)
            gbar += g_step
        gtilde += g_step
        
    # Close files
    violence_plot.close()
    profit_plot.close()
    drug_plot.close()
    qhatbar_plot.close()
    qhatN_plot.close()
    
    print "Done."
    

    # Old 2D graph
    exit() # Comment if you want to enable (fixes below are required first)
    with open('violence_corrupt', 'w') as corrupt_out:
        with open('violence_crackdown', 'w') as crackdown_out:
            Gmax = 0.
            while Gmax <= 3.:
                print 'Gmax=', Gmax
                gbar = 0.
                gtilde = Gmax
                V_corrupt, theta_corrupt, M = calc_eqbm(omegatilde, gbar, gtilde)
                # NEED TO FIX
#                 violence_corrupt = calc_M(theta_corrupt, gbar, omegatilde, gtilde)
#                 corrupt_out.write(str(Gmax) + " " + str(violence_corrupt) + "\n")
                
                gbar = Gmax
                V_crackdown, theta_crackdown, M = calc_eqbm(omegatilde, gbar, gtilde)
                # NEED TO FIX
#                 violence_crackdown = calc_M(theta_crackdown, gbar, omegatilde, gtilde)
#                 crackdown_out.write(str(Gmax) + " " + str(violence_crackdown) + "\n")
                
                print "   theta_corr=", theta_corrupt, " V_corrupt=", V_corrupt, " theta_crackdown=", theta_crackdown, " V_crackdown=", V_crackdown
                
    #             print 'gtilde = ', gtilde, ' I got:', Vbar, thetabar, M
    #             print 'violence=', violence, F(thetabar)
                Gmax += 0.05
