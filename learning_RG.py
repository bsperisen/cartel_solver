import numpy as np

import plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import scipy.stats
import scipy.optimize
import math
import sys
import sqlite3

# Government policy space
G = 12.1 # Government capacity
g_inc = 1.0 # Government capability increments
g_grid = np.arange(0.0, G, g_inc)
n_g = len(g_grid)

# Specify probability distributions
r_mean, r_range = (10.0, 0.1)
c_mean, c_range = (1.0, 0.1)
delta_mean, delta_range = (0.90, 0.02)
kappa_mean, kappa_range = (0.5, 0.1)
eta_mean, eta_range = (0.01, 0.01)

# Intercept of demand curve.
r = r_mean
# Marginal cost of quantity.
c = c_mean
# Number of traffickers to kill.
kappa = kappa_mean
# Cost of violence.
eta = eta_mean # 0.02 * kappa * (.25 * (r - c) - kappa)
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.0001
# infinity
infinity = float("inf")
zeta = 0.1
# Discount factor
delta = delta_mean
# Periods of Punishment
T = 20

# Create string identifier for this parameterization to avoid any weird floating point errors when choosing what
# results to load from database.
param_id = 'G' + str(G) + 'g_inc' + str(g_inc) + 'rm' + str(r_mean) + 'rr' + str(r_range) + 'cm' + str(c_mean) + 'cr' + str(c_range) \
           + 'dm' + str(delta_mean) + 'dr' + str(delta_range) + 'km' + str(kappa_mean) + 'kr' + str(kappa_range) \
           + 'em' + str(eta_mean) + 'er' + str(eta_range)


C0 = (5 * r - 2 * c) / 12
C1 = ((r + 2 * c) ** 2) / 144

# Needed in case we need to reset epsilon
orig_epsilon = epsilon

class action:

    def __init__(self, a, q, s, n=False):
        self.alpha = a
        self.qi = q
        self.si = s
        self.isnash = n

    def to_string(self):
        if self.isnash:
            return "NASH: (alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"
        else:
            return "(alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"

    def is_equal(self, action):
        if self.alpha == action.alpha and self.qi == action.qi:
            if self.si == action.si:
                return True
        return False


def calc_price(action, g):
    return max(r - 2 * (action.qi - action.si * kappa - g), 0.0)


def calc_payoff(action, g):
    price = calc_price(action, g)
    payoff = price * (action.qi - action.si * kappa - g) - c * action.qi - action.si * eta
    return max(payoff, 0.0)


def calculate_nash_values(gset):
    payoffs = {}
    alphas = {}
    quants = {}
    for g in gset:
        alpha = calculate_nash_alpha(g, 1.0, 0, 1)
        payoff = calculate_payoff(g, alpha)
        payoffs[g] = max(0.0, payoff)
        alphas[g] = alpha
        quants[g] = calculate_q(g, alpha)
    return payoffs, alphas, quants


def f(theta):
    if theta <= 0:
        return 0
    x = scipy.stats.norm.pdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x / (zeta * theta)


def F(theta):
    if theta <= 0:
        return 0
    if theta == infinity:
        return 1
    x = scipy.stats.norm.cdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x


def lambda_func(theta):
    return (delta * f(theta) * theta) / (6 - 6 * delta)


def psi(theta, delta_V):
    return C0 - math.sqrt(C1 + lambda_func(theta) * delta_V)


def V_tilde(w, nash):
    return (1 - (delta ** T)) * nash + (delta ** T) * w


def RHS(theta, delta_v):
    # print('deltaV', delta_v)
    x = 4 * psi(theta, delta_v) - r + c
    denom = (12 * zeta ** 2) * math.sqrt(C1 + lambda_func(theta) * delta_v)
    full_frac = ((-1) * x * (math.log(theta) + 0.5 * zeta ** 2)) / denom
    return full_frac - 1

eps_eq_zero = 1.e-10

def b_hat(w, g, nash):
    delta_v = w - V_tilde(w, nash)
    if delta_v <= eps_eq_zero:
        comp2 = (1 - delta) * nash + delta * w
        return comp2, 0.0, action(oneshot_nash_alphas[g[0]], oneshot_nash_quantities[g[0]], 1, n=True)
    else:
        theta_interior, result = scipy.optimize.brentq(RHS, epsilon, math.exp(-0.5 * zeta ** 2), args=(delta_v), full_output=True)
        if result.converged:
            if 0 >= (1 - delta) * (kappa * psi(theta_interior, delta_v) - eta) - delta * delta_v:
                si_bar = 0
            else:
                si_bar = 1
            q_interior = psi(theta_interior, delta_v) + g[0] + kappa * si_bar
            eq_action = action(1, q_interior, si_bar)
            comp1 = (1 - delta) * calc_payoff(eq_action, g[0]) + \
                (delta * ((1 - F(theta_interior)) * w + F(theta_interior) * V_tilde(w, nash)))
            comp2 = (1 - delta) * nash + delta * w
            if comp1 > comp2:
                return comp1, theta_interior, eq_action
            else:
                return comp2, 0.0, action(oneshot_nash_alphas[g[0]], oneshot_nash_quantities[g[0]], 1, n=True)
        else:
            print("Did not converge: ", w, g, nash)
            v = (1 - delta) * nash + delta * w
            return v, 0, action(oneshot_nash_alphas[g[1]], oneshot_nash_quantities[g[1]], 1)


def calc_avg_disc_violence(action, theta_bar, g):
    V_Nash = (oneshot_nash_alphas[g] ** 2) * 2 * kappa
    numerator = (1 - delta) * 2 * kappa * action.si * (action.alpha ** 2) + \
        delta * F(theta_bar) * (1 - (delta ** T)) * V_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


def calc_avg_disc_q_hat(action, theta_bar, g_bar, g_tilde):
    Q_hat = (2 * action.alpha) * (action.qi - g_bar - (action.alpha * action.si * kappa))
    Q_Nash = (2 * oneshot_nash_alphas[g_tilde]) * (oneshot_nash_quantities[g_tilde] -
                                                   g_tilde - oneshot_nash_alphas[g_tilde] * kappa)
    numerator = (1 - delta) * Q_hat + delta * F(theta_bar) * (1 - (delta ** T)) * Q_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    # if g_bar == 10:
    #     print(action.to_string(), theta_bar, g_bar, g_tilde)
    #     print(Q_Nash, Q_hat)
    #     print(denominator)
    return numerator / denominator


def calculate_nash_alpha(g, alpha, lb, ub):
    payoff = calculate_payoff(g, alpha)
    if g <= mix_thresh:
        return 1.0
    if (ub - lb) <= epsilon:
        return alpha
    if payoff < 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, lb, alpha)
    elif payoff > 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, alpha, ub)
    else:
        return alpha


def calculate_q(g, alpha):
    if alpha == 0:
        return 0
    else:
        return (r - c + ((2 + alpha) * g) + (3 * alpha * kappa)) / (2 + alpha)


def calculate_payoff(g, alpha):
    qi = calculate_q(g, alpha)

    # Calculate the utility of the case where both Cartels are 'In'
    in_price = r - 2 * (qi - g - kappa)
    in_utility = in_price * (qi - g - kappa) - c * qi - eta
    in_utility = in_utility * alpha

    # Calculate the utility of the case where one Cartel is In and the other is
    # 'Out'
    out_price = r - qi + g
    out_utility = out_price * (qi - g) - c * qi
    out_utility = out_utility * (1 - alpha)
    return in_utility + out_utility

# Note: this is copied from the loadbalance function from supergametools
# Returns a list of the number of g values assigned to processors.
def loadbalance(n, p):
    inc = int(n/p)
    R = n - inc*p

    load = [int(inc) for i in range(p)]

    for i in range(int(R)):
        load[i] += 1

    load.append(0)
    load.sort()
    return load


# Returns tuple (e, theta, eq_action)
# e is payoff (float)
# theta is thetabar threshold (float)
# eq_action is abar object (Action)
def calc_rep_eqbm(g_bar, g_tilde, epsilon):
    g_tuple = (g_bar, g_tilde)
    if g_bar > g_tilde:
        print("Error: passed g_bar > g_tilde")
        sys.exit(1)

    # print('parameters are: {} {}'.format(r, c))

    monopoly_action = action(1, 0.25 * (r - c) + g_bar, 0)
    w = calc_payoff(monopoly_action, g_bar)
    w_ub = w
    theta = 0.
    eq_action = action(oneshot_nash_alphas[g_bar], oneshot_nash_quantities[g_bar], 1, n=True)
    w_lb = 0.
    found_lb = False
    while (not found_lb) and (w_ub > eps_eq_zero):
        diff = infinity
        iter = 0
        while diff >= epsilon:
            v, theta, eq_action = b_hat(w, g_tuple, oneshot_nash_payoffs[g_tilde])
            diff = w - v
            w = v
            iter += 1
        w_ub = w
        w_lb = w - epsilon
        check, misc, misc2 = b_hat(w_lb, g_tuple, oneshot_nash_payoffs[g_tilde])
        if check >= w_lb:
            found_lb = True
        else:
            w = w_lb
            epsilon /= 1.5
        if epsilon < eps_eq_zero:
            epsilon = orig_epsilon

    e = 0.5 * (w_ub + w_lb)
    return (e, theta, eq_action)

class GovtLearner:
    def __init__(self, g_grid, learn_epsilon=0.3):
        self.T = 0 # Count of total experiments
        self.viol_upperbound = 2*(kappa_mean+kappa_range)
        self.g_grid = g_grid
        n_g = len(g_grid)
        self.viol_values = np.zeros((n_g, n_g)) + self.viol_upperbound # Keeps track of violence averages for each policy
        self.counts = np.zeros((n_g, n_g), dtype=np.int) # Tracks count of times policy was tried
        self.learn_epsilon = learn_epsilon

    # Method to pick policy
    # Returns list of tuples (g_bar_ind, g_tilde_ind) with length processes
    # algor = 'UCB' (default) or 'epsgreedy'
    # processes can be set higher than 1 to experiment with multiple in parallel
    # allow_coordination set to True if coordinated policies are allowed
    def pick_policy(self, algor='UCB', processes=1,  allow_coordination=False):
        tuple_list = []
        if algor == 'UCB':
            if allow_coordination:
                print("Not implemented yet")
                sys.exit(1)
            else: # Uncoordinated
                counts = self.counts.diagonal()
                if counts.min() == 0: # If there are any policies untried, need to do those first.
                    pbest_policies = np.argpartition(counts, processes)[:processes]
                else:
                    # Need to convert violence to positive rewards by subtracting from upper bound.
                    reward_avg = self.viol_upperbound - self.viol_values.diagonal()
                    Bvals = reward_avg + np.sqrt(2*np.log(self.T) / counts)
                    pbest_policies = np.argpartition(Bvals, -processes)[-processes:]
                tuple_list = [(pbest_policies[i], pbest_policies[i]) for i in range(len(pbest_policies))]
                    # g_bar_ind = Bvals.argmax()
                # g_tilde_ind = g_bar_ind

        else: # epsgreedy algorithm
            g_bar_ind = g_tilde_ind = 0
            print('implementation needs updating to tuple list')
            sys.exit(1)
            rand = np.random.rand()
            if rand < self.learn_epsilon: # Experimentation
                if allow_coordination:
                    g_tilde_ind = np.random.randint(len(self.g_grid))
                    g_bar_ind = np.random.randint(g_tilde+1)
                else:
                    g_bar_ind = np.random.randint(len(self.g_grid))
                    g_tilde_ind = g_bar_ind

            else: # Pick greedy action
                if allow_coordination:
                    g_bar_ind = 1
                    g_tilde_ind = 0
                    while g_bar_ind > g_tilde_ind: # Make sure we choose no invalid policies
                        nonzero_inds = np.nonzero(self.viol_values == self.viol_values.min())
                        g_ind_ind = np.random.randint(len(nonzero_inds))
                        g_bar_ind = self.g_grid[nonzero_inds[0][g_ind_ind]]
                        g_tilde_ind = self.g_grid[nonzero_inds[1][g_ind_ind]]
                else:
                    uncoord_viol_values = self.viol_values.diagonal()
                    g_bar_ind = np.random.choice(np.flatnonzero(uncoord_viol_values == uncoord_viol_values.min()))
                    g_tilde_ind = g_bar_ind

        return tuple_list

    def update_values(self, g_bar_ind, g_tilde_ind, violence):
        self.T += 1
        self.counts[g_bar_ind, g_tilde_ind] += 1
        if self.counts[g_bar_ind, g_tilde_ind] == 1: # Need this due to +infty values
            self.viol_values[g_bar_ind, g_tilde_ind] = violence
        else:
            self.viol_values[g_bar_ind, g_tilde_ind] = self.viol_values[g_bar_ind, g_tilde_ind] + (1./self.counts[g_bar_ind, g_tilde_ind]) * (violence - self.viol_values[g_bar_ind, g_tilde_ind])




# Main Routine
# Parallelize over different policies
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# Check that bounds on kappa, eta, satisfied
min_r = r_mean - r_range
max_c = c_mean + c_range
min_c = c_mean - c_range
min_rmc = min_r - max_c
min_kappa = kappa_mean - kappa_range
max_kappa = kappa_mean + kappa_range
max_eta = eta_mean + eta_range
kappa_bound = min((1./6.)*min_rmc, (2./3)*math.sqrt(-((1./3.)*((min_rmc)+c)) + math.sqrt(((1./3.)*(min_rmc+min_c))**2+(1./3.)*min_rmc**2)))
eta_bound = 0.25*min_rmc*min_kappa

if max_kappa > kappa_bound or max_eta > eta_bound:
    print('bounds unsatisfied')
    sys.exit(1)

# Create distributions
r_dist = scipy.stats.triang(0.5, loc=r_mean-r_range, scale = 2*r_range)
c_dist = scipy.stats.triang(0.5, loc=c_mean-c_range, scale = 2*c_range)
delta_dist = scipy.stats.triang(0.5, loc=delta_mean-delta_range, scale = 2*delta_range)
kappa_dist = scipy.stats.triang(0.5, loc=kappa_mean-kappa_range, scale = 2*kappa_range)
eta_dist = scipy.stats.triang(0.5, loc=eta_mean-eta_range, scale = 2*eta_range)

# Build learner object
learner = GovtLearner(g_grid)

# Load database
dbconn = sqlite3.connect("previous_runs.db")
cursor = dbconn.cursor()
create_command = """
CREATE TABLE IF NOT EXISTS runs (
id_num INTEGER PRIMARY KEY,
param_id TEXT,
g_bar_ind INTEGER,
g_tilde_ind INTEGER,
r REAL,
c REAL,
delta REAL,
kappa REAL,
eta REAL,
e REAL,
thetabar REAL,
violence REAL,
drug_consumption REAL
);"""

try:
    cursor.execute(create_command)

    # Load any previous runs
    cursor.execute("SELECT g_bar_ind, g_tilde_ind, violence FROM runs WHERE param_id = ?", (param_id,))
    rows = cursor.fetchall()
    if rank == 0:
        print('Found {} previous runs'.format(len(rows)))
        for row in rows:
            g_bar_ind, g_tilde_ind, violence = row
            learner.update_values(g_bar_ind, g_tilde_ind, violence)


except sqlite3.Error as e:
    print(e)
    sys.exit(1)

X, Y = np.meshgrid(g_grid, g_grid)
oneshot_nash_payoffs, oneshot_nash_alphas, oneshot_nash_quantities = calculate_nash_values(g_grid)

n_rounds = 50000
results_buf = np.zeros(size) # Buffer for this process
results = np.zeros(size) # Will collect the results from all processes
n_params = 5
params_buf = np.zeros((size, n_params))
params = np.zeros((size,n_params))

n_eqbm_results = 4
eqbm_results_buf = np.zeros((size,n_eqbm_results))
eqbm_results = np.zeros((size,n_eqbm_results))
for round in range(n_rounds):
    results_buf = np.zeros(size)
    results = np.zeros(size)
    params_buf = np.zeros((size, 5))
    params = np.zeros((size,5))
    eqbm_results_buf = np.zeros((size,4))
    eqbm_results = np.zeros((size,4))

    # Draw new instances of parameters
    r = r_dist.rvs(1)
    c = c_dist.rvs(1)
    delta = delta_dist.rvs(1)
    kappa = kappa_dist.rvs(1)
    eta = eta_dist.rvs(1)

    # Update global variables that depend on the parameters
    mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
    C0 = (5 * r - 2 * c) / 12
    C1 = ((r + 2 * c) ** 2) / 144

    policies_to_try = learner.pick_policy(algor='UCB', processes=size)
    g_bar_ind, g_tilde_ind = policies_to_try[rank]
    g_bar = g_grid[g_bar_ind]
    g_tilde = g_grid[g_tilde_ind]

    e, thetabar, eq_action = calc_rep_eqbm(g_bar, g_tilde, epsilon)
    violence = calc_avg_disc_violence(eq_action, thetabar, g_tilde)
    drug_consumption = calc_avg_disc_q_hat(eq_action, thetabar, g_bar, g_tilde)

    # Collect results and update
    results_buf[rank] = violence
    params_buf[rank,:] = np.array([r, c, delta, kappa, eta]).flatten()
    eqbm_results_buf[rank,:] = np.array([e, thetabar, violence, drug_consumption])
    # print('proc {} results_buf {}'.format(rank, results_buf))
    comm.Allreduce(results_buf, results)
    comm.Allreduce(params_buf, params)
    comm.Allreduce(eqbm_results_buf, eqbm_results)
    # print('proc {} results {} policies to try {}'.format(rank, results, policies_to_try))
    sys.stdout.flush()
    for proc in range(size):
        g_bar_ind, g_tilde_ind = policies_to_try[proc]
        violence = float(results[proc])
        learner.update_values(g_bar_ind, g_tilde_ind, violence)
        # if rank == 0:
        #     print('proc {} updating {} counts {}'.format(rank, g_bar_ind, learner.counts.diagonal()))

        # Only root should talk to database, print output
        if rank == 0:
            insert_command = """
INSERT INTO runs (param_id, g_bar_ind, g_tilde_ind, r, c, delta, kappa, eta, e, thetabar, violence, drug_consumption) 
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

            insert_id_policy = [param_id, int(g_bar_ind), int(g_tilde_ind)]
            insert_params = [float(params[proc,i]) for i in range(n_params)]
            insert_eqbm_results = [float(eqbm_results[proc, i]) for i in range(n_eqbm_results)]

            insert_data = [param_id, int(g_bar_ind), int(g_tilde_ind), violence] # TODO check if int typecast is actually necessary here

            cursor.execute(insert_command, insert_id_policy + insert_params + insert_eqbm_results)
            dbconn.commit()
            updated_viol = learner.viol_values[g_bar_ind, g_tilde_ind]

    if rank == 0:
        print('experiment round {}, tried policy {}, updated violence {}'.format(round, policies_to_try, results))
        print('violence grid {}'.format(learner.viol_values.diagonal()))
        print('counts grid {}'.format(learner.counts.diagonal()))

    sys.stdout.flush()

sys.exit(1)


### OLD CODE ###

gshape = X.shape
# Buffers for each thread to fill in
bounds_buf = np.zeros(shape=X.shape, dtype=float)
eq_theta_buf = np.zeros(shape=X.shape, dtype=float)
eq_violence_buf = np.zeros(shape=X.shape, dtype=float)
eq_qhat_buf = np.zeros(shape=X.shape, dtype=float)
diag_payoffs_buf = np.zeros(len(g_grid))
diag_violence_buf = np.zeros(len(g_grid))
diag_qhat_buf = np.zeros(len(g_grid))

# The arrays where we'll combine the results.
bounds = np.zeros(shape=X.shape, dtype=float)
eq_theta = np.zeros(shape=X.shape, dtype=float)
eq_violence = np.zeros(shape=X.shape, dtype=float)
eq_qhat = np.zeros(shape=X.shape, dtype=float)
diag_payoffs = np.zeros(len(g_grid))
diag_violence = np.zeros(len(g_grid))
diag_qhat = np.zeros(len(g_grid))


# Uncomment for 2D case
#slices = loadbalance(len(g_grid)**2, size)
# print('size', size, len(g_grid))
# loadbalance(len(g_grid), 2) # Testing
slices = loadbalance(len(g_grid), size)
cumslices = np.cumsum(np.array(slices))
slice = slices[rank+1] # slices contains the number of jobs each process will do.

# print('slices {}'.format(slices))

# Pick 2D vs 3D
if True:
    for s in range(slice):
        # Convert the 1-d index (cumslices[rank]+s) to 2-d indices.
        g_ind = int(cumslices[rank]) + s
        g_bar = g_grid[g_ind]
        g_tilde = g_grid[g_ind]

        g_tuple = (g_bar, g_tilde)
        print('process {} g_tuple {}'.format(rank, g_tuple))
        if g_bar <= g_tilde:
            e, theta, eq_action = calc_rep_eqbm(g_bar, g_tilde, epsilon)

            diag_payoffs_buf[g_ind] = e
            diag_violence_buf[g_ind] = calc_avg_disc_violence(eq_action, theta, g_tuple[1])
            diag_qhat_buf[g_ind] = calc_avg_disc_q_hat(eq_action, theta, g_tuple[0], g_tuple[1])

else:
    for s in range(slice):
        # Convert the 1-d index (cumslices[rank]+s) to 2-d indices.
        row_num = int((int(cumslices[rank]) + s) / len(g_grid))
        col_num = (int(cumslices[rank]) + s) % len(g_grid)
        g_bar = g_grid[row_num]
        g_tilde = g_grid[col_num]

        g_tuple = (g_bar, g_tilde)
        print('gtuple', g_tuple)
        if g_bar <= g_tilde:
            e, theta, eq_action = calc_rep_eqbm(g_bar, g_tilde, epsilon)

            bounds_buf[row_num][col_num] = e
            eq_theta_buf[row_num][col_num] = F(theta)
            eq_violence_buf[row_num][col_num] = calc_avg_disc_violence(eq_action, theta, g_tuple[1])
            eq_qhat_buf[row_num][col_num] = calc_avg_disc_q_hat(eq_action, theta, g_tuple[0], g_tuple[1])

            if row_num == col_num:
                diag_payoffs_buf[row_num] = e
                diag_violence_buf[row_num] = calc_avg_disc_violence(eq_action, theta, g_tuple[1])
                diag_qhat_buf[row_num] = calc_avg_disc_q_hat(eq_action, theta, g_tuple[0], g_tuple[1])



# Collect results from all the processes
comm.Allreduce(bounds_buf, bounds)
comm.Allreduce(eq_theta_buf, eq_theta)
comm.Allreduce(eq_violence_buf, eq_violence)
comm.Allreduce(eq_qhat_buf, eq_qhat)

comm.Allreduce(diag_payoffs_buf, diag_payoffs)
comm.Allreduce(diag_violence_buf, diag_violence)
comm.Allreduce(diag_qhat_buf, diag_qhat)

# Only plot on root process
if rank == 0:
    pay_plot = open('payoff_plot', 'w')
    viol_plot = open('viol_plot', 'w')
    qhat_plot = open('qhat_plot', 'w')
    for i in range(len(g_grid)):
        g = g_grid[i]
        pay_plot.write(str(g) + ' ' + str(diag_payoffs[i]) + "\n")
        viol_plot.write(str(g) + ' ' + str(diag_violence[i]) + "\n")
        qhat_plot.write(str(g) + ' ' + str(diag_qhat[i]) + "\n")

    pay_plot.close()
    viol_plot.close()
    qhat_plot.close()
    sys.exit(1)

    # Plot Avg Disc Violence
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_violence
    )]
    layout = go.Layout(
        title="Average Discounted Violence",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Violence"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="Violence3D.html")

    # Plot F(Theta)
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_theta
    )]
    layout = go.Layout(
        title="Probability of Triggering Punishment",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="F(Theta)"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="FThetaBar3D.html")

    # Plot Quantity Delivered
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_qhat
    )]
    layout = go.Layout(
        title="Average Discounted Quantity Delivered",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Quantity Delivered"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="QHat3D.html")

    # Plot Payoffs
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=bounds
    )]
    layout = go.Layout(
        title="Equilibrium Payoffs",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Payoff"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="Payoff3D.html")
