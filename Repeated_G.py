import numpy as np

import plotly as py
import plotly.graph_objs as go
import matplotlib.pyplot as plt

import scipy.stats
import scipy.optimize
import math
import sys
import sqlite3

# Government policy space
G = 24.0  # Government capacity
g_inc = 0.2  # Government capability increments
g_grid = np.arange(0.0, G, g_inc)
n_g = len(g_grid)

# Specify probability distributions
r_mean, r_range = (10.0, 0.1)
c_mean, c_range = (1.0, 0.1)
delta_mean, delta_range = (0.90, 0.02)
kappa_mean, kappa_range = (0.5, 0.1)
eta_mean, eta_range = (0.01, 0.01)

# Intercept of demand curve.
r = r_mean
# Marginal cost of quantity.
c = c_mean
# Number of traffickers to kill.
kappa = kappa_mean
# Cost of violence.
eta = eta_mean  # 0.02 * kappa * (.25 * (r - c) - kappa)
# The mixing threshold
mix_thresh = ((1/9) * (r - c) ** 2 + eta - c * kappa) / c
# Precision Constant
epsilon = 0.0001
# infinity
infinity = float("inf")
zeta = 0.1
# Discount factor
delta = delta_mean
# Periods of Punishment
T = 20

# Create string identifier for this parameterization to avoid any weird floating point errors when choosing what
# results to load from database.
param_id = 'G' + str(G) + 'g_inc' + str(g_inc) + 'rm' + str(r_mean) + 'rr' + str(r_range) + 'cm' + str(c_mean) + 'cr' + str(c_range) \
           + 'dm' + str(delta_mean) + 'dr' + str(delta_range) + 'km' + str(kappa_mean) + 'kr' + str(kappa_range) \
           + 'em' + str(eta_mean) + 'er' + str(eta_range)


C0 = (5 * r - 2 * c) / 12
C1 = ((r + 2 * c) ** 2) / 144

# Needed in case we need to reset epsilon
orig_epsilon = epsilon


# Epsilon used to check if values (in particular payoffs) are "equal" from zero
eps_eq_zero = 1.e-10


class action:

    def __init__(self, a, q, s, n=False):
        self.alpha = a
        self.qi = q
        self.si = s
        self.isnash = n

    def to_string(self):
        if self.isnash:
            return "NASH: (alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"
        else:
            return "(alpha = " + str(self.alpha) + ", qi = " + str(self.qi) + ", si = " + str(self.si) + ")"

    def is_equal(self, action):
        if self.alpha == action.alpha and self.qi == action.qi:
            if self.si == action.si:
                return True
        return False


def calc_price(action, g):
    return max(r - 2 * (action.qi - action.si * kappa - g), 0.0)


def calc_payoff(action, g):
    price = calc_price(action, g)
    payoff = price * (action.qi - action.si * kappa - g) - c * action.qi - action.si * eta
    return max(payoff, 0.0)


def calculate_nash_values(gset):
    payoffs = {}
    alphas = {}
    quants = {}
    for g in gset:
        g = math.floor(g * 10)/10
        alpha = calculate_nash_alpha(g, 1.0, 0, 1)
        payoff = calculate_payoff(g, alpha)
        payoffs[g] = max(0.0, payoff)
        alphas[g] = alpha
        quants[g] = calculate_q(g, alpha)
    return payoffs, alphas, quants


def f(theta):
    if theta <= eps_eq_zero:
        return 0
    x = scipy.stats.norm.pdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x / (zeta * theta)


def F(theta):
    if theta <= 0:
        return 0
    if theta == infinity:
        return 1
    x = scipy.stats.norm.cdf((math.log(theta) + 0.5 * zeta ** 2) / zeta)
    return x


def lambda_func(theta):
    return (delta * f(theta) * theta) / (6 - 6 * delta)


def psi(theta, delta_V):
    return C0 - math.sqrt(C1 + lambda_func(theta) * delta_V)


def V_tilde(w, nash):
    return (1 - (delta ** T)) * nash + (delta ** T) * w


def RHS(theta, delta_v):
    # print('deltaV', delta_v)
    x = 4 * psi(theta, delta_v) - r + c
    denom = (12 * zeta ** 2) * math.sqrt(C1 + lambda_func(theta) * delta_v)
    full_frac = ((-1) * x * (math.log(theta) + 0.5 * zeta ** 2)) / denom
    return full_frac - 1


def b_hat(w, g, nash):
    delta_v = w - V_tilde(w, nash)
    if delta_v <= eps_eq_zero:
        comp2 = (1 - delta) * nash + delta * w
        return comp2, 0.0, action(oneshot_nash_alphas[g[0]], oneshot_nash_quantities[g[0]], 1, n=True)
    else:
        theta_interior, result = scipy.optimize.brentq(
            RHS, epsilon, math.exp(-0.5 * zeta ** 2), args=(delta_v), full_output=True)
        if result.converged:
            if 0 >= (1 - delta) * (kappa * psi(theta_interior, delta_v) - eta) - delta * delta_v:
                si_bar = 0
            else:
                si_bar = 1
            q_interior = psi(theta_interior, delta_v) + g[0] + kappa * si_bar
            eq_action = action(1, q_interior, si_bar)
            comp1 = (1 - delta) * calc_payoff(eq_action, g[0]) + \
                (delta * ((1 - F(theta_interior)) * w + F(theta_interior) * V_tilde(w, nash)))
            comp2 = (1 - delta) * nash + delta * w
            if comp1 > comp2:
                return comp1, theta_interior, eq_action
            else:
                return comp2, 0.0, action(oneshot_nash_alphas[g[0]], oneshot_nash_quantities[g[0]], 1, n=True)
        else:
            print("Did not converge: ", w, g, nash)
            v = (1 - delta) * nash + delta * w
            return v, 0, action(oneshot_nash_alphas[g[1]], oneshot_nash_quantities[g[1]], 1)


def calc_avg_disc_violence(action, theta_bar, g):
    V_Nash = (oneshot_nash_alphas[g] ** 2) * 2 * kappa
    numerator = (1 - delta) * 2 * kappa * action.si * (action.alpha ** 2) + \
        delta * F(theta_bar) * (1 - (delta ** T)) * V_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


def calc_avg_disc_q_hat(action, theta_bar, g_bar, g_tilde):
    Q_hat = (2 * action.alpha) * (action.qi - g_bar - (action.alpha * action.si * kappa))
    Q_Nash = (2 * oneshot_nash_alphas[g_tilde]) * (oneshot_nash_quantities[g_tilde] -
                                                   g_tilde - oneshot_nash_alphas[g_tilde] * kappa)
    numerator = (1 - delta) * Q_hat + delta * F(theta_bar) * (1 - (delta ** T)) * Q_Nash
    denominator = 1 - (delta * (1 - F(theta_bar))) - (F(theta_bar) * (delta ** (T + 1)))
    return numerator / denominator


def calculate_nash_alpha(g, alpha, lb, ub):
    payoff = calculate_payoff(g, alpha)
    if g <= mix_thresh:
        return 1.0
    if (ub - lb) <= epsilon:
        return alpha
    if payoff < 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, lb, alpha)
    elif payoff > 0:
        return calculate_nash_alpha(g, (lb + ub) / 2, alpha, ub)
    else:
        return alpha


def calculate_q(g, alpha):
    if alpha == 0:
        return 0
    else:
        return (r - c + ((2 + alpha) * g) + (3 * alpha * kappa)) / (2 + alpha)


def calculate_payoff(g, alpha):
    qi = calculate_q(g, alpha)

    # Calculate the utility of the case where both Cartels are 'In'
    in_price = r - 2 * (qi - g - kappa)
    in_utility = in_price * (qi - g - kappa) - c * qi - eta
    in_utility = in_utility * alpha

    # Calculate the utility of the case where one Cartel is In and the other is
    # 'Out'
    out_price = r - qi + g
    out_utility = out_price * (qi - g) - c * qi
    out_utility = out_utility * (1 - alpha)
    return in_utility + out_utility

# Returns tuple (e, theta, eq_action)
# e is payoff (float)
# theta is thetabar threshold (float)
# eq_action is abar object (Action)


def calc_rep_eqbm(g_bar, g_tilde, epsilon):
    g_tuple = (g_bar, g_tilde)
    if g_bar > g_tilde:
        print("Error: passed g_bar > g_tilde")
        sys.exit(1)

    # print('parameters are: {} {}'.format(r, c))

    monopoly_action = action(1, 0.25 * (r - c) + g_bar, 0)
    w = calc_payoff(monopoly_action, g_bar)
    w_ub = w
    theta = 0.
    eq_action = action(oneshot_nash_alphas[g_bar], oneshot_nash_quantities[g_bar], 1, n=True)
    w_lb = 0.
    found_lb = False
    while (not found_lb) and (w_ub > eps_eq_zero):
        diff = infinity
        iter = 0
        while diff >= epsilon:
            v, theta, eq_action = b_hat(w, g_tuple, oneshot_nash_payoffs[g_tilde])
            diff = w - v
            w = v
            iter += 1
        w_ub = w
        w_lb = w - epsilon
        check, misc, misc2 = b_hat(w_lb, g_tuple, oneshot_nash_payoffs[g_tilde])
        if check >= w_lb:
            found_lb = True
        else:
            w = w_lb
            epsilon /= 1.5
        if epsilon < eps_eq_zero:
            epsilon = orig_epsilon

    e = 0.5 * (w_ub + w_lb)
    return (e, theta, eq_action)


# Note: this is copied from the loadbalance function from supergametools
# Returns a list of the number of g values assigned to processors.
def loadbalance(n, p):
    inc = n/p
    R = n - inc*p

    load = [int(inc) for i in range(p)]

    for i in range(int(R)):
        load[i] += 1

    load.append(0)
    load.sort()
    return load


# Main Routine
X, Y = np.meshgrid(g_grid, g_grid)
oneshot_nash_payoffs, oneshot_nash_alphas, oneshot_nash_quantities = calculate_nash_values(g_grid)

# Buffers for each thread to fill in
bounds_buf = np.zeros(shape=X.shape, dtype=float)
eq_theta_buf = np.zeros(shape=X.shape, dtype=float)
eq_violence_buf = np.zeros(shape=X.shape, dtype=float)
eq_qhat_buf = np.zeros(shape=X.shape, dtype=float)

# The arrays where we'll combine the results.
bounds = np.zeros(shape=X.shape, dtype=float)
eq_theta = np.zeros(shape=X.shape, dtype=float)
eq_violence = np.zeros(shape=X.shape, dtype=float)
eq_qhat = np.zeros(shape=X.shape, dtype=float)

precomputed = np.zeros(shape=X.shape, dtype=np.bool)
pre_bounds = np.zeros(shape=X.shape, dtype=float)
pre_eq_theta = np.zeros(shape=X.shape, dtype=float)
pre_eq_violence = np.zeros(shape=X.shape, dtype=float)
pre_eq_qhat = np.zeros(shape=X.shape, dtype=float)

n_eqbm_results = 4
eqbm_results_buf = np.zeros(n_eqbm_results)
eqbm_results = np.zeros(n_eqbm_results)

# Parallelize over different g's
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
slices = loadbalance(len(g_grid)**2, size)
cumulative_slices = np.cumsum(np.array(slices))
slice = slices[rank+1]  # slices contains the number of jobs each process will do.


# Load database
dbconn = sqlite3.connect("previous_runs.db")
cursor = dbconn.cursor()
create_command = """
CREATE TABLE IF NOT EXISTS runs (
id_num INTEGER PRIMARY KEY,
param_id TEXT,
g_bar_ind INTEGER,
g_tilde_ind INTEGER,
r REAL,
c REAL,
delta REAL,
kappa REAL,
eta REAL,
e REAL,
thetabar REAL,
violence REAL,
drug_consumption REAL
);"""

try:
    cursor.execute(create_command)
    # Load any previous runs
    cursor.execute(
        "SELECT g_bar_ind, g_tilde_ind, e, thetabar, violence, drug_consumption FROM runs WHERE param_id = ?", (param_id,))
    rows = cursor.fetchall()
    if rank == 0:
        print('Found {} previous runs'.format(len(rows)))
        for row in rows:
            g_bar_ind, g_tilde_ind, e, thetabar, violence, drug_consumption = row
            precomputed[g_bar_ind, g_tilde_ind] = True
            pre_bounds[g_bar_ind, g_tilde_ind] = e
            pre_eq_theta[g_bar_ind, g_tilde_ind] = thetabar
            pre_eq_violence[g_bar_ind, g_tilde_ind] = violence
            pre_eq_qhat[g_bar_ind, g_tilde_ind] = drug_consumption


except sqlite3.Error as e:
    print(e)
    sys.exit(1)

comm.Barrier()

for s in range(slice):
    # Convert the 1-d index (cumslices[rank]+s) to 2-d indices.
    g_bar_ind = int((int(cumulative_slices[rank]) + s) / len(g_grid))
    g_tilde_ind = (int(cumulative_slices[rank]) + s) % len(g_grid)

    g_bar = math.floor(g_grid[g_bar_ind]*10)/10
    g_tilde = math.floor(g_grid[g_tilde_ind]*10)/10

    g_tuple = (g_bar, g_tilde)

    if g_bar > g_tilde or precomputed[g_bar_ind, g_tilde_ind]:  # No need to recompute previous runs
        continue

    e, thetabar, eq_action = calc_rep_eqbm(g_bar, g_tilde, epsilon)
    print('gtuple', g_tuple)
    sys.stdout.flush()

    violence = calc_avg_disc_violence(eq_action, thetabar, g_tilde)
    drug_consumption = calc_avg_disc_q_hat(eq_action, thetabar, g_bar, g_tilde)

    bounds_buf[g_bar_ind][g_tilde_ind] = e
    eq_theta_buf[g_bar_ind][g_tilde_ind] = F(thetabar)
    eq_violence_buf[g_bar_ind][g_tilde_ind] = calc_avg_disc_violence(
        eq_action, thetabar, g_tuple[1])
    eq_qhat_buf[g_bar_ind][g_tilde_ind] = calc_avg_disc_q_hat(
        eq_action, thetabar, g_tuple[0], g_tuple[1])

    # Insert data into DB.
    insert_command = """
INSERT INTO runs (param_id, g_bar_ind, g_tilde_ind, r, c, delta, kappa, eta, e, thetabar, violence, drug_consumption)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    insert_id_policy = [param_id, int(g_bar_ind), int(g_tilde_ind)]
    insert_params = [r, c, delta, kappa, eta]
    insert_eqbm_results = [e, thetabar, violence, drug_consumption]

    # insert_data = [param_id, int(g_bar_ind), int(g_tilde_ind), violence] # TODO check if int typecast is actually necessary here

    cursor.execute(insert_command, insert_id_policy + insert_params + insert_eqbm_results)
    dbconn.commit()
    # updated_viol = learner.viol_values[g_bar_ind, g_tilde_ind]

    # if row_num == col_num:
    #     diag_payoffs_buf[row_num] = w_ub
    #     diag_violence_buf[row_num] = calc_avg_disc_violence(eq_action, theta, g_tuple[1])
    #     diag_qhat_buf[row_num] = calc_avg_disc_q_hat(eq_action, theta, g_tuple[0], g_tuple[1])


# Collect results from all the processes
comm.Allreduce(bounds_buf, bounds)
comm.Allreduce(eq_theta_buf, eq_theta)
comm.Allreduce(eq_violence_buf, eq_violence)
comm.Allreduce(eq_qhat_buf, eq_qhat)

bounds += pre_bounds
eq_theta += pre_eq_theta
eq_violence += pre_eq_violence
eq_qhat += pre_eq_qhat

uncoord_payoffs = np.diag(bounds)
uncoord_theta = np.diag(eq_theta)
uncoord_violence = np.diag(eq_violence)
uncoord_qhat = np.diag(eq_qhat)

coord_payoffs = np.array([i for i in bounds[0]])
coord_theta = np.array([i for i in eq_theta[0]])
coord_violence = np.array([i for i in eq_violence[0]])
coord_qhat = np.array([i for i in eq_qhat[0]])

bounds[np.tril_indices(n_g, k=-1)] = np.NaN
eq_theta[np.tril_indices(n_g, k=-1)] = np.NaN
eq_violence[np.tril_indices(n_g, k=-1)] = np.NaN
eq_qhat[np.tril_indices(n_g, k=-1)] = np.NaN

# Only plot on root process
if rank == 0:
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_violence
    )]
    layout = go.Layout(
        title="Average Discounted Violence",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Violence"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="Violence3D.html")

    # Plot F(Theta)
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_theta
    )]
    layout = go.Layout(
        title="Probability of Triggering Punishment",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="F(Theta)"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="FThetaBar3D.html")

    # Plot Quantity Delivered
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=eq_qhat
    )]
    layout = go.Layout(
        title="Average Discounted Quantity Delivered",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Quantity Delivered"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="QHat3D.html")

    # Plot Payoffs
    surf = [go.Surface(
        colorscale='Viridis',
        x=g_grid,
        y=g_grid,
        z=bounds
    )]
    layout = go.Layout(
        title="Equilibrium Payoffs",
        autosize=True,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
        ),
        scene=dict(
            xaxis=dict(
                title="G Tilde"
            ),
            yaxis=dict(
                title="G Bar"
            ),
            zaxis=dict(
                title="Avg. Disc. Payoff"
            )
        )
    )
    fig = go.Figure(data=surf, layout=layout)
    py.offline.plot(fig, filename="Payoff3D.html")

    plt.plot(g_grid, coord_payoffs)
    plt.xlabel("g_tilde (g_bar = 0)")
    plt.ylabel("Payoffs")
    plt.title("Equilibrium Payoffs: Coordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("coord_payoff.png")

    plt.clf()
    plt.plot(g_grid, coord_violence)
    plt.xlabel("g_tilde (g_bar = 0)")
    plt.ylabel("Violence")
    plt.title("Equilibrium Violence: Coordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("coord_violence.png")

    plt.clf()
    plt.plot(g_grid, coord_qhat)
    plt.xlabel("g_tilde (g_bar = 0)")
    plt.ylabel("Qhat")
    plt.title("Equilibrium Quantity Delivered: Coordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("coord_qhat.png")

    plt.clf()
    plt.plot(g_grid, coord_theta)
    plt.xlabel("g_tilde (g_bar = 0)")
    plt.ylabel("F(theta)")
    plt.title("Equilibrium Probability of Triggering Punishment: Coordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("coord_theta.png")

    plt.clf()
    plt.plot(g_grid, uncoord_payoffs)
    plt.xlabel("g_tilde = g_bar")
    plt.ylabel("Payoffs")
    plt.title("Equilibrium Payoffs: Uncoordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("uncoord_payoff.png")

    plt.clf()
    plt.plot(g_grid, uncoord_violence)
    plt.xlabel("g_tilde = g_bar")
    plt.ylabel("Violence")
    plt.title("Equilibrium Violence: Uncoordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("uncoord_violence.png")

    plt.clf()
    plt.plot(g_grid, uncoord_qhat)
    plt.xlabel("g_tilde = g_bar")
    plt.ylabel("Qhat")
    plt.title("Equilibrium Quantity Delivered: Uncoordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("uncoord_qhat.png")

    plt.clf()
    plt.plot(g_grid, uncoord_theta)
    plt.xlabel("g_tilde = g_bar")
    plt.ylabel("F(theta)")
    plt.title("Equilibrium Probability of Triggering Punishment: Uncoordinated Policy")
    plt.grid(True)
    plt.xticks(np.arange(0.0, 30.0, 2))
    plt.savefig("uncoord_theta.png")
